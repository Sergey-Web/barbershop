var link = document.querySelector(".login");
var popup = document.querySelector(".connect");
var close = document.querySelector(".connect_close");
var login = popup.querySelector("[name=login]");
var password = popup.querySelector("[name=password]");
var form = popup.querySelector("form");
var storage = localStorage.getItem("login");


link.addEventListener("click", function(event) {
    event.preventDefault();
    popup.classList.add("connect_show");

    if(storage) {
        login.value = storage;
        password.focus();
    } else {
        login.focus();
    }
});

close.addEventListener("click", function(event) {
    event.preventDefault();
    popup.classList.remove("connect_show");
});

form.addEventListener("submit", function(event){
    if(!(login.value && password.value)) {
        event.preventDefault();
        console.log("Нужно ввести логин и пароль");
    } else {
        localStorage.setItem("login", login.value);
    }
});


window.addEventListener("keydown", function(event){
    if(event.keyCode == 27) {
        if(popup.classList.contains("connect_show")) {
            popup.classList.remove("connect_show");
        }
    }
});

