var contact = document.querySelector(".btn_address");
var address = document.querySelector(".map");
var map_close = document.querySelector(".map_close");

 contact.addEventListener("click", function(event){
    event.preventDefault();
    address.classList.add("map_show");
});

 map_close.addEventListener("click", function(event){
    event.preventDefault();
    address.classList.remove("map_show");
});

window.addEventListener("keydown", function(event){
    if(event.keyCode == 27) {
        if(address.classList.contains("map_show")) {
            address.classList.remove("map_show");
        }
    }
});